package br.quixada.ufc.si.exec;



import br.quixada.ufc.si.model.Cliente;
import br.quixada.ufc.si.model.Contrato;
import br.quixada.ufc.si.model.Operadora;
import br.quixada.ufc.si.model.Vendedor;
import java.time.LocalDate;
import javafx.util.converter.LocalDateStringConverter;


public class PrincipalImprimivel {
    
    public static void main(String[] args) {
        Cliente cliente1 = new Cliente() {};
        Cliente cliente2 = new Cliente() {};
        Cliente cliente3 = new Cliente() {};
        
        LocalDate a = LocalDate.of(1998, 8, 12);
        Contrato contrato1 = new Contrato();
        Contrato contrato2 = new Contrato();
        Contrato contrato3 = new Contrato();
        
        Vendedor vendedor1 = new Vendedor();
        Vendedor vendedor2 = new Vendedor();
        Vendedor vendedor3 = new Vendedor();
        
        Operadora operador = new Operadora();
    }
}
