
package br.quixada.ufc.si.model;


public class Vendedor extends Funcionario{
    
    public Vendedor(){
        
    }
    
    public void realizarVenda(float valorContrato){
       float salarioVendedor = 100;
        
       salarioVendedor = (float) (salarioVendedor + (valorContrato * 0.5));
       darBonificacao();
       
    }

    @Override
    public void darBonificacao() {
       float salarioVendedor = 100;
       salarioVendedor = (salarioVendedor * 5/100) + salarioVendedor;
    }
}
