package br.quixada.ufc.si.model;

import java.time.LocalDate;


public class ClientePessoaJuridica extends Cliente{
    private String cnpj;
    private LocalDate dataAbertura;
    
    public ClientePessoaJuridica(){
        
    }
    
    public ClientePessoaJuridica(String endereco,String cnpj,LocalDate dataAbertura){
        super();
        this.cnpj = cnpj;
        this.dataAbertura = dataAbertura;
    }

    
    public String getCnpj() {
        return cnpj;
    }

   
    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

   
    public LocalDate getDataAbertura() {
        return dataAbertura;
    }

   
    public void setDataAbertura(LocalDate dataAbertura) {
        this.dataAbertura = dataAbertura;
    }
    
     public void toString(String endereco,String cnpj,LocalDate dataAbertura) {
        System.out.println(
                "Endereco: " + this.getNome()+
                "CNPJ: " + this.getCnpj()+ "Data de abertura: " + this.getDataAbertura()
        );
    }
}
