
package br.quixada.ufc.si.model;


public class Pessoa {
    private String nome;

    public Pessoa(){
        
    }
    public Pessoa(String nome){
        this.nome = nome;
    }
    
    public String getNome() {
        return nome;
    }

    
    public void setNome(String nome) {
        this.nome = nome;
    }
    
    
    
    public void toString(String nome) {
        System.out.println(
                "Nome: " + this.getNome()
        );
    }
    
}
