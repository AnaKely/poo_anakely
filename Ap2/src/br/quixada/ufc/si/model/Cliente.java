package br.quixada.ufc.si.model;


public abstract class Cliente extends Pessoa{
    private String endereco;

    public Cliente(){
        
    }
    
    public Cliente(String nome, String endereco){
        super();
        this.endereco = endereco;
    }
    
    public String getEndereco() {
        return endereco;
    }

    
    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }
    public void toString(String nome,String enderaco) {
        System.out.println(
                "Nome: " + this.getNome()+
                "Enereco: " + this.getEndereco()
        );
    }
    
}
