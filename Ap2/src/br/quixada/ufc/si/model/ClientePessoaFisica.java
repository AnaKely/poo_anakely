package br.quixada.ufc.si.model;
import java.time.LocalDate;


public class ClientePessoaFisica extends Cliente{
    private String cpf;
    private LocalDate data_nasc;
    
    public ClientePessoaFisica(){
        
    }
    public ClientePessoaFisica(String endereco,String cpf,LocalDate data_nasc){
        super();
        this.cpf = cpf;
        this.data_nasc = data_nasc;
    }

    
    public String getCpf() {
        return cpf;
    }

    
    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

   
    public LocalDate getData_nasc() {
        return data_nasc;
    }

    public void setData_nasc(LocalDate data_nasc) {
        this.data_nasc = data_nasc;
    }
    
    public void toString(String endereco,String cpf,LocalDate data_nasc) {
        System.out.println(
                "Endereco: " + this.getEndereco()+
                "CPF: " + this.getCpf()+ "Data de nascimento: " + this.getData_nasc()
        );
    }
}
