package br.quixada.ufc.si.model;

import br.quixada.ufc.si.interfaces.Imprimivel;
import java.util.ArrayList;


public class Operadora implements Imprimivel{
    private ArrayList<Contrato> contrato = new ArrayList <>();
    private ArrayList<Cliente> clientes = new ArrayList <>();
    
    private int CodOperadora;
    private String nome;
    
    
    public Operadora(){
        
    }
    public Operadora(int CodOperadora,String nome){
        this.CodOperadora = CodOperadora;
        this.nome = nome;
    }

    public ArrayList<Contrato> getContrato() {
        return contrato;
    }

    public void setContrato(ArrayList<Contrato> contrato) {
        this.contrato = contrato;
    }

  
    public ArrayList<Cliente> getClientes() {
        return clientes;
    }

    public void setClientes(ArrayList<Cliente> clientes) {
        this.clientes = clientes;
    }

   
    public int getCodOperadora() {
        return CodOperadora;
    }

    
    public void setCodOperadora(int CodOperadora) {
        this.CodOperadora = CodOperadora;
    }

   
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
    public void toString(int CodOperadora,String nome) {
        System.out.println(
                "Codigo Operadora: " + this.getCodOperadora()
                +"Nome: " + this.getNome()
                
        );
    }

    @Override
    public void MostrarDados() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

   

}
