
import java.util.Scanner;


public class Questao06 {
    
     public static Scanner scan = new Scanner(System.in);
    public static void main(String[] args) {
        
        int a = scan.nextInt();
        System.out.print("Fatorial e: " + retornaFatorial(a));
    
    
}
    public static int retornaFatorial(int a){
        int i;
        int fat = 1;
        for(i = 1; i <= a; i++){
           fat = fat * i;
        }
        return fat;
    }
    
}
