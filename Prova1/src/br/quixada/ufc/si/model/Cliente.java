
package br.quixada.ufc.si.model;


public class Cliente {
    private String nomeCliente;
    private String cpf;
    private String tipo;
    
    public Cliente(){
        
    }
    
    public Cliente(String nome, String cpf,String tipo){
        this.nomeCliente = nomeCliente;
        this.cpf = cpf;
        this.tipo = tipo;
    }
    
    public String setNome(){
        return this.nomeCliente;
    }
    public String setCpf(){
        return this.cpf;
    }
     public String setTipo(){
        return this.tipo;
    }
     
    public String getNome(String nome){
        return this.nomeCliente = nomeCliente;
    } 
    public String getCpf(String cpf){
        return this.cpf = cpf;
    } 
    public String getTipo(String tipo){
        return this.tipo = tipo;
    } 
    
    public float solicitarDesconto(float valor){
        
        if(tipo == "Estudante"){
           float desconto1 = valor * 0.5f;
           return desconto1;
        }
        else if(tipo == "Policial"){
           float desconto2 = valor * 0.2f;
           return desconto2;
        }
        else if(tipo == "Professor"){
           float desconto3 = valor * 0.1f;
           return desconto3;
        } 
        else if(tipo == "idoso"){
           float desconto4 = valor * 0.5f;
           return desconto4;
        } 
        return 0;
        
    }
    
    public String toString(){
        String cliente = "";
            cliente = "Nome do cliente: "+this.nomeCliente+"\n"+"CPF: "+
                    this.cpf+"\n"+"Tipo: "+this.tipo;
            return cliente;
    }
}
