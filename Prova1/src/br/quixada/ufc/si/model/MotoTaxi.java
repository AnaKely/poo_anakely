
package br.quixada.ufc.si.model;

import com.sun.org.apache.xalan.internal.xsltc.cmdline.getopt.GetOpt;

public class MotoTaxi {
    private String nome;
    private String cnh;
    private String placa;
    private float nota;
    
    public MotoTaxi(){
        
    }
    
    public MotoTaxi(String nome,String cnh,String placa, float nota){
        this.nome = nome;
        this.cnh = cnh;
        this.placa = placa;
        this.nota = nota;
    }
    
    public String getNome(){
        return this.nome;
    }
    
    public String getCnh(){
        return this.cnh;
    }
    
    public String getPlaca(){
        return this.placa;
    }
    
    public float getNota(){
        return this.nota;
    }
    
    public String setNome(String nome){
        return this.nome = nome;
    }
    
    public String setCnh(String cnh){
        return this.cnh = cnh;
    }
    
    public String setPlaca(String placa){
        return this.placa = placa;
    }
    public float setNota(float nota){
        return this.nota = nota;
    }
    
    public void realizarCorrida(Cliente cliente, Corrida corrida){
        System.out.println(nome);
        System.out.println(cliente.getNome("J"));
        System.out.println(nota);
        System.out.println(corrida.getPartida());
        System.out.println(corrida.getDestino());
        
        
    }
    
    public String toString(){
        String MotoTaxi = "";
            MotoTaxi = "Nome do moto taxi: "+this.nome+"\n"+"CNH: "+
                    this.cnh+"\n"+"Placa :"+this.placa+"\n"+"Nota: "+this.nota;
            return MotoTaxi;
    }
}
