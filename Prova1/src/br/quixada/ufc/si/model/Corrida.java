
package br.quixada.ufc.si.model;

public class Corrida {
   
    private String partida;
    private String destino;
    private float precoKM;
    private float precoCorrida;
    
    public Corrida(){
        
    }
    public Corrida(String partida, String destino,float precoKM, float precoCorrida){
        this.partida = partida;
        this.destino = destino;
        this.precoKM = precoKM;
        this.precoCorrida = precoCorrida;
    }
    
    public String getPartida(){
        return this.partida;
    }
    
    public String getDestino(){
        return this.destino;
    }
    
    public float getprecoKM(){
        return this.precoKM;
    }
    public float getprecoCorrida(){
        return this.precoCorrida;
    }
    
    public String setPartida(String partida){
        return this.partida = partida;
    }
    
    public String setDestino(String partida){
        return this.destino = destino;
    }
    
    public float setPrecokm(float precoKM){
        return this.precoKM = precoKM;
    }
    
    public float setPrecocorrida(float precoCorrida){
        return this.precoCorrida = precoCorrida;
    }
    public float calcularValorCorrida(int distancia){
        float valorCorrida = (distancia * precoKM) + 5;
        return valorCorrida;
    }
    
     public String toString(){
        String corrida = "";
            corrida = "NPartida: "+this.partida+"\n"+"Destino: "+
                    this.destino+"\n"+"Preco de km: "+this.precoKM+
                    "\n"+"Preco da corrida: "+this.precoCorrida;
            return corrida;
    }
}
