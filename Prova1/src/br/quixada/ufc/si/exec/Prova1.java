
package br.quixada.ufc.si.exec;

import br.quixada.ufc.si.model.Cliente;
import br.quixada.ufc.si.model.Corrida;
import br.quixada.ufc.si.model.MotoTaxi;


public class Prova1 {
    static float desconto;
    
    public static void main(String[] args) {
        MotoTaxi motoTaxi1 = new MotoTaxi("ANA", "123", "2fd43", 1);
        MotoTaxi motoTaxi2 = new MotoTaxi("Carlos", "343", "daf5", 2);
        
        Corrida corrida1 = new Corrida("Quixada", "Cedro", 20.0f, 10.0f);
        Corrida corrida2 = new Corrida("Cedro", "Quixada", 10.0f, 20.0f);
        Corrida corrida3 = new Corrida("Pedra Branca", "Cruzeta", 15.0f, 10.0f);
        
        Cliente cliente1 = new Cliente("Kely", "128263", "Estudante");
        Cliente cliente2 = new Cliente("Kessia", "128623", "Idosa");
        Cliente cliente3 = new Cliente("Maria", "14263", "Policial");
        
        
        //cliente1.solicitarDesconto(20);
        System.out.println(cliente1.solicitarDesconto(20));
        System.out.println(cliente2.solicitarDesconto(50));
        System.out.println(cliente3.solicitarDesconto(30));
        
        desconto = cliente1.solicitarDesconto(20);
        System.out.println("Valor do desconto: "+desconto);
        desconto = cliente2.solicitarDesconto(50);
        System.out.println("Valor do desconto: "+desconto);
        desconto = cliente3.solicitarDesconto(30);
        System.out.println("Valor do desconto: "+desconto);
       
        
        motoTaxi1.realizarCorrida(cliente1, corrida1);
        motoTaxi2.realizarCorrida(cliente2, corrida2);
       
        System.out.println(corrida1.calcularValorCorrida(5));
        System.out.println(corrida2.calcularValorCorrida(4));
        System.out.println(corrida3.calcularValorCorrida(5));
        
       
        
        
        
    }
    
}
