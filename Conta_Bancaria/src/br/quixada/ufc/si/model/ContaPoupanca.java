
package br.quixada.ufc.si.model;


public class ContaPoupanca extends ContaBancaria implements Imprimivel {

    private double taxa = 0.055;
    
    public ContaPoupanca(){
      
    }

    public ContaPoupanca(String conta, double saldo) {
        super(conta, saldo);
    }

    @Override
    public void sacar(double valor) {
        if(valor <= this.getSaldo()) {
            this.setSaldo(this.getSaldo() - valor);
            this.setSaldo(this.getSaldo() - valor * taxa);
        }
        else System.out.println("Não foi feito o saque! Seu valor tem que ser menor ou igual ao saldo.");
    }

    @Override
    public void depositar(double valor) {
        this.setSaldo(this.getSaldo() + valor);
        this.setSaldo(this.getSaldo() - valor * taxa);
    }

    @Override
    public void MostrarDados() {
        System.out.println(
                "Conta: " + this.getNumeroDaConta() +
                "\nSaldo: " + this.getSaldo() +
                "\nTaxa: " + this.taxa * 100 + "%\n"
        );
    }
}

