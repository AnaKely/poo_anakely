
package br.quixada.ufc.si.model;

public abstract class ContaBancaria {

    private String Numero_Da_Conta;
    private double saldo;

    public ContaBancaria(String conta, double saldo) {
        this.Numero_Da_Conta = Numero_Da_Conta;
        this.saldo = saldo;
    }
    
    public ContaBancaria() {
        
    }
 
    public String getNumeroDaConta() {
        return Numero_Da_Conta;
    }

    void setNumeroDaConta(String Numero_Da_Conta) {
        this.Numero_Da_Conta = Numero_Da_Conta;
    }

    double getSaldo() {
        return saldo;
    }

    void setSaldo(double saldo) {
        this.saldo = saldo;
    }
    
    public abstract void sacar(double valor);

    public abstract void depositar(double valor);

    public void transferir(double valor, ContaBancaria conta) {
        this.sacar(valor);
        conta.setSaldo(conta.getSaldo() + valor);
    }

    public void MostrarDados() {
        System.out.println(
                "Conta: " + this.getNumeroDaConta() +
                "\nSaldo: " + this.getSaldo());
    }
}

