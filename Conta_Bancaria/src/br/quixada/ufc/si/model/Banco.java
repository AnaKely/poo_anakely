
package br.quixada.ufc.si.model;

import java.util.ArrayList;


public class Banco implements Imprimivel {

    private ArrayList<ContaBancaria> contas = new ArrayList <>();

    public void adicionar(ContaBancaria conta) {
        contas.add(conta);
    }

    public void remover(ContaBancaria conta) {
       contas.remove(conta);
   }

   public ContaBancaria procurarConta(String conta) {
        for(ContaBancaria resultado : contas){
            if(resultado.getNumeroDaConta().equals(conta)){
                return resultado;
            }
        }
        return null;
    }

    @Override
    public void MostrarDados() {
        for (ContaBancaria conta : contas) {
            System.out.println(conta.getNumeroDaConta());
        }
    }
}

