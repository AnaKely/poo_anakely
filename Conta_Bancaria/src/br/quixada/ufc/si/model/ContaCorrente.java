
package br.quixada.ufc.si.model;



    public class ContaCorrente extends ContaBancaria implements Imprimivel {

    private double limite = 50;

    public ContaCorrente(String conta, double saldo) {
        super(conta, saldo);
    }

    
    @Override
    public void depositar(double valor) {
        this.setSaldo( this.getSaldo() + valor );
    }
    
    @Override
    public void sacar(double valor ) {
        if ( valor <= this.getSaldo()+this.limite ) {
            this.setSaldo ( this.getSaldo() - valor );
        }
        else System.out.println("Erro. Você passou do seu limite de saque.");
    }
    @Override
    public void MostrarDados() {
        System.out.println(
                "Conta: " + this.getNumeroDaConta() +
                "\nSaldo: " + this.getSaldo() +
                "\nLimite: " + this.limite + "\n"
        );
    }

  
}


