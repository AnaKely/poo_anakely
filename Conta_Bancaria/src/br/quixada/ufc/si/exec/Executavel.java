
package br.quixada.ufc.si.exec;
import br.quixada.ufc.si.model.ContaBancaria;
import br.quixada.ufc.si.model.Banco;
import br.quixada.ufc.si.model.ContaCorrente;
import br.quixada.ufc.si.model.ContaPoupanca;
import java.util.Scanner;

public class Executavel {
    private static void Menu(Banco banco) {
        
        Scanner input = new Scanner(System.in);
        
        int criar = 1;
        int remover = 2;
        int selecionar = 3;
        int gerar = 4;

        
         System.out.println(
                "\nMENU" +
                "\nEscolha uma opção abaixo: " +
                "\n1 : Criar conta" +
                "\n2 : Selecionar conta" +
                "\n3 : Remover conta" +
                "\n4 : Gerar relatório" +
                "\n5 : Finalizar");

         int opcaoCliente = input.nextInt();
         
         if(opcaoCliente == criar){
             Criar(banco);
         }
        else if(opcaoCliente == selecionar) {

            Selecionar(banco);
        }
        else if(opcaoCliente == remover) {

            Remover(banco);
        }
        else if(opcaoCliente == gerar) {

            banco.MostrarDados();
            Menu(banco);
        }
    }
    
    private static void Criar(Banco banco) {

        Scanner input = new Scanner(System.in);

        System.out.println("Tipo de conta:" +
                           "\n1 : Conta Poupança" +
                           "\n2 : Conta Corrente");

        int opcaoCliente = input.nextInt();

        String Numero_Da_Conta = CriarConta(opcaoCliente, banco);

        if(banco.procurarConta(Numero_Da_Conta) != null) {

            System.out.println("Conta criada.");
            Menu(banco);
        }
        else System.out.println("Erro ao criar a conta.");
             Menu(banco);
    }
        
    private static String CriarConta(int opcaoCliente, Banco banco) {

        Scanner input = new Scanner(System.in);

        int ContaPoupanca = 1;
        int ContaCorrente = 2;

        if(opcaoCliente == ContaCorrente) {

            System.out.println("Informe o número da conta: ");
            String Numero_Da_Conta = input.next();

            System.out.println("Informe o saldo da conta: ");
            double saldo = input.nextDouble();

            ContaCorrente conta = new ContaCorrente(Numero_Da_Conta, saldo);
            banco.adicionar(conta);

            return Numero_Da_Conta;
        }
       else if(opcaoCliente == ContaPoupanca) {

            System.out.println("Informe o número da conta: ");
            String Numero_Da_Conta = input.next();

            System.out.println("Informe o saldo da conta: ");
            double saldo = input.nextDouble();
           ContaPoupanca conta = new ContaPoupanca(Numero_Da_Conta, saldo);
            banco.adicionar(conta);

            return Numero_Da_Conta;
        }
        else {
            System.out.println("Conta não encontrada!");
            Menu(banco);

            return null;
        }
    }
    private static void Selecionar(Banco banco) {

        Scanner input = new Scanner(System.in);

        System.out.println("Informe o número da conta: ");
        String Numero_Da_Conta = input.next();

        int depositar  = 1;
        int sacar   = 2;
        int gerar_relatorio = 3;
        int transferir  = 4;
        int voltar  = 5;

        if(banco.procurarConta(Numero_Da_Conta) != null) {
            
            System.out.println("Escolha uma opção:" +
                               "\n1 : Depositar" +
                               "\n2 : Sacar" +
                               "\n3 : Tranferir" +
                               "\n4 : Gerar relatório" +
                               "\n5 : Voltar");

            

        }
        int opcaoCliente = input.nextInt();
        
        if(opcaoCliente == depositar) {

                System.out.println("Valor desejado: ");
                double valor = input.nextDouble();
                banco.procurarConta(Numero_Da_Conta).depositar(valor);

                System.out.println("Valor depositado.");
                Menu(banco);
            }
            else if(opcaoCliente == sacar) {

                System.out.println("Valor desejado: ");
                double valor = input.nextDouble();
                banco.procurarConta(Numero_Da_Conta).sacar(valor);

                System.out.println("A quantia foi sacada.");
                Menu(banco);
            }
        else if(opcaoCliente == transferir) {

                System.out.println("Conta que irá receber a transferência: ");
                String contaRecebe = input.next();

                if(banco.procurarConta(contaRecebe) != null) {

                    System.out.println("Informe o valor: ");
                    double valor = input.nextDouble();
                    banco.procurarConta(Numero_Da_Conta).transferir(valor, banco.procurarConta(contaRecebe));

                    System.out.println("Transferência feita.");
                    Menu(banco);
                }
                else {
                    System.out.println("Conta não encontrada.");
                    Menu(banco);
                }
            }
            else if(opcaoCliente == gerar_relatorio) {
                banco.procurarConta(Numero_Da_Conta).MostrarDados();
                Menu(banco);
            }
            else if(opcaoCliente == voltar) {
                Menu(banco);
            }
     else {
            System.out.println("Conta não encontrada.");
            Menu(banco);
        }
     
    }
    private static void Remover(Banco banco) {

        Scanner input = new Scanner(System.in);

        System.out.println("Informe o número da conta: ");
        String conta = input.next();

        if(banco.procurarConta(conta) != null) {
            banco.remover(banco.procurarConta(conta));

            if(banco.procurarConta(conta) == null) {

                System.out.println("Conta removida.");
                Menu(banco);
            }
            else {
                System.out.println("Houve.");
                Menu(banco);
            }
        }
        else {
            System.out.println("Conta não encontrada.");
            Menu(banco);
        }
    }

    public static void main(String[] args) {

        Banco banco = new Banco();
        Menu(banco);
    }
}




