
package br.quixada.ufc.si.exec;

import br.quixada.ufc.si.model.ContaCorrente;
import br.quixada.ufc.si.model.ContaPoupanca;
import br.quixada.ufc.si.model.Relatorio;

public class Principal {
    public static void main( String[] args ) {

        
        ContaPoupanca pessoa = new ContaPoupanca( "789000-3", 6900 );
        ContaCorrente pessoa2 = new ContaCorrente( "841316-4", 400.50 );
        pessoa.sacar( 2350.50 );
        pessoa2.sacar( 5000 );

        Relatorio relatorio = new Relatorio();
        relatorio.gerarRelatorio(pessoa);
        relatorio.gerarRelatorio(pessoa2);


    }

}
