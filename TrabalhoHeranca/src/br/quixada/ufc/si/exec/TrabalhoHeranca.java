
package br.quixada.ufc.si.exec;

import br.quixada.ufc.si.model.*;


public class TrabalhoHeranca {

    
    public static void main(String[] args) {
        Fornecedor fornecedor = new Fornecedor("Jose", "Rua Oscar Barbosa, 910", "943162", (float) 1000.0, 500);
        Empregado empregado = new Empregado("Ana", "hajrea", "hafda", 1, (float)3000.0, 500);
        Administrador administrador = new Administrador("Carla", "gar251","getda", 5, (float)7000.0, 200);
        Operario operario = new Operario("Silvia", "614dajs", "8251", 6, (float) 3000.0, 20, 50, 500);
        Vendedor vendedor = new Vendedor("João", "da4172", "98731sd", 2, (float) 8000.0, 30, 10, 30);
                
        System.out.print(fornecedor.obterSlado(200, 100));
        fornecedor.TestarFornecedor("Ana", "Rua oscar", "t51683", 20, 30);
        
        System.out.println(empregado.CalcularSalario(30, 8, 20));
        empregado.TestarEmpregado("Silivia","Rua coracao","421813", 50, 1, 98);
        
        System.out.println(administrador.CalcularSalario(21, 23, 10));
        administrador.TestarAdministrador("Carlos", "rua fradte", "tfahska", 2, (float) 3000.0, 102);
        
        System.out.println(operario.CalcularSalario(20, 20, 30, 60));
        operario.TestarOperario("João", "rua grada", "864dara", 1, (float) 4000.0, 200, 1000, 500);
        
        System.out.println(vendedor.CalcularSalario(12, 20, 50, 100));
        vendedor.TestarVendedor("Maria", "rayao082", "3252719", 2, (float) 4000.0, 23, 40, 10);
    }
    
}
