
package br.quixada.ufc.si.model;

public class Operario extends Empregado{
    private float valorProducao;
    private float comissao;

    public float getValorProducao() {
        return valorProducao;
    }

    public void setValorProducao(float valorProducao) {
        this.valorProducao = valorProducao;
    }

    public float getComissao() {
        return comissao;
    }

    public void setComissao(float comissao) {
        this.comissao = comissao;
    }
    public Operario(){
        
    }
    public Operario(String nome,String endereco,String telefone,int codigoSetor,float salarioBase,float imposto,float valorProducao,float comissao){
        super(nome,endereco,telefone,codigoSetor,salarioBase,imposto);
        this.valorProducao = valorProducao;
        this.comissao = comissao;
    }
    public void TestarOperario(String nome,String endereco,String telefone,int codigoSetor,float salarioBase,float imposto,float valorProducao,float comissao){
        System.out.println(nome);
        System.out.println(endereco);
        System.out.println(telefone);
        System.out.println(codigoSetor);
        System.out.println(salarioBase);
        System.out.println(imposto);
         System.out.println(valorProducao);
         System.out.println(comissao);
       
    }
        
    
    public float CalcularSalario(int dia, int horas, float valorHora,float comissao){
        float salario = ((dia * horas * valorHora)+ comissao);
        return salario;
    }
}
