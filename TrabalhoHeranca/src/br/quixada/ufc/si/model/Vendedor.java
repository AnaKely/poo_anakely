
package br.quixada.ufc.si.model;


public class Vendedor extends Empregado{
    private float valorVendas;
    private float comissao;

    public float getValorVendas() {
        return valorVendas;
    }

    public void setValorVendas(float valorVendas) {
        this.valorVendas = valorVendas;
    }

    public float getComissao() {
        return comissao;
    }

    public void setComissao(float comissao) {
        this.comissao = comissao;
    }
    
    public Vendedor(){
        
    }
    public Vendedor(String nome,String endereco,String telefone,int codigoSetor,float salarioBase,float imposto,float valorVendas,float comissao){
        super(nome,endereco,telefone,codigoSetor,salarioBase,imposto);
        this.valorVendas = valorVendas;
        this.comissao = comissao;
    }
    
    public void TestarVendedor(String nome,String endereco,String telefone,int codigoSetor,float salarioBase,float imposto,float valorVendas,float comissao){
        System.out.println(nome);
        System.out.println(endereco);
        System.out.println(telefone);
        System.out.println(codigoSetor);
        System.out.println(salarioBase);
        System.out.println(imposto);
        System.out.println(comissao);
        System.out.println(valorVendas);
    }
    public float CalcularSalario(int dia, int horas, float valorHora,float comissao){
        float salario = ((dia * horas * valorHora)+ comissao);
        return salario;
    }
    
}
