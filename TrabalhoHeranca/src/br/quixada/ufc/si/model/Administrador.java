
package br.quixada.ufc.si.model;


public class Administrador extends Empregado{
    private String ajudaDeCusto;

   
    public String getAjudaDeCusto() {
        return ajudaDeCusto;
    }

    public void setAjudaDeCusto(String ajudaDeCusto) {
        this.ajudaDeCusto = ajudaDeCusto;
    }
    
    public Administrador(){
        
    }
    public Administrador(String nome,String endereco,String telefone,int codigoSetor,float salarioBase,float imposto){
        super(nome,endereco,telefone,codigoSetor,salarioBase,imposto);
        this.ajudaDeCusto = ajudaDeCusto;
    }
    
     public float CalcularSalario(int dia, int horas, float valorHora){
        float salario = (dia * horas * valorHora);
        return salario;
    }
     
    public void TestarAdministrador(String nome,String endereco,String telefone,int codigoSetor,float salarioBase,float imposto){
        System.out.println(nome);
        System.out.println(endereco);
        System.out.println(telefone);
        System.out.println(codigoSetor);
        System.out.println(salarioBase);
        System.out.println(imposto);
        System.out.println(ajudaDeCusto);
        
    }
    
    
}
