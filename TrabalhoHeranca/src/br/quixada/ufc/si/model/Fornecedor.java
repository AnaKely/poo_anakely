
package br.quixada.ufc.si.model;


public class Fornecedor extends Pessoa{
    private float valorCredito;
    private float valorDivida;

    
    public float getValorCredito() {
        return valorCredito;
    }

    public void setValorCredito(float valorCredito) {
        this.valorCredito = valorCredito;
    }

    public float getValorDivida() {
        return valorDivida;
    }

    public void setValorDivida(float valorDivida) {
        this.valorDivida = valorDivida;
    }
    
    public Fornecedor(){
        
    }
    public Fornecedor(String nome,String endereco,String telefone,float valorCredito,float valorDivida){
        super(nome,endereco,telefone);
        this.valorCredito = valorCredito;
        this.valorDivida = valorDivida;
    }
    public float obterSlado(float valorCredito, float valorDivida){
        float saldo = valorCredito - valorDivida;
        return saldo;
    }
    
    public void TestarFornecedor(String nome,String endereco,String telefone,float valorCredito,float valorDivida){
        System.out.println(nome);
        System.out.println(endereco);
        System.out.println(telefone);
        System.out.println(valorCredito);
        System.out.println(valorDivida);
    }
    
}
