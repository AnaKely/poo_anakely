
package br.quixada.ufc.si.model;

public class Empregado extends Pessoa{
   private int codigoSetor;
   private float salarioBase;
   private float imposto;

    
    public int getCodigoSetor() {
        return codigoSetor;
    }

    public void setCodigoSetor(int codigoSetor) {
        this.codigoSetor = codigoSetor;
    }

    public float getSalarioBase() {
        return salarioBase;
    }

    public void setSalarioBase(float salarioBase) {
        this.salarioBase = salarioBase;
    }

    public float getImposto() {
        return imposto;
    }

    public void setImposto(float imposto) {
        this.imposto = imposto;
    }
    
    public Empregado(){
        
    }
    public Empregado(String nome,String endereco,String telefone,int codigoSetor,float salarioBase,float imposto){
        super(nome,endereco,telefone);
        this.codigoSetor = codigoSetor;
        this.imposto = imposto;
        this.salarioBase = salarioBase;
    }
    public void TestarEmpregado(String nome,String endereco,String telefone,int codigoSetor,float salarioBase,float imposto){
        System.out.println(nome);
        System.out.println(endereco);
        System.out.println(telefone);
        System.out.println(codigoSetor);
        System.out.println(salarioBase);
        System.out.println(imposto);
        
    }
    public float CalcularSalario(int dia, int horas, float valorHora){
        float salario = (dia * horas * valorHora);
        return salario;
    }
   
   
}
