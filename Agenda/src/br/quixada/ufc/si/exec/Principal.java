package br.quixada.ufc.si.exec;
import br.quixada.ufc.si.model.Agenda;
import br.quixada.ufc.si.model.Pessoa;
import br.quixada.ufc.si.model.PessoaFisica;
import br.quixada.ufc.si.model.PessoaJuridica;
import java.time.LocalDate;
import java.util.Scanner;



public class Principal {
	public static Scanner teclado = new Scanner(System.in);
	public static void main(String[] args) {
		Agenda agenda = new Agenda();
		LocalDate a = LocalDate.of(1998, 8, 12);
		LocalDate b = LocalDate.of(1800, 2, 22);
		LocalDate c = LocalDate.of(2000, 2, 13);
		LocalDate d = LocalDate.of(1999, 6, 7);
		Pessoa a1 = new PessoaFisica("Ana Maria", "Rua oscar", "lkely@gmail.com", 1, d, "Casada");
		Pessoa a2 = new PessoaFisica("Marta", "RUA JOAO", "lll@gmail.com", 5, d, "Solteira");
		Pessoa a3 = new PessoaJuridica("Arlete", "Rua vivi", "ka@gmail.com", 3, 1, "sda");
		Pessoa a4 = new PessoaJuridica("Carla", "Rua joao", "ihad@gmail.com", 6, 2, "rqeda");
                        
		agenda.adicionar(a1);
		agenda.adicionar(a4);
		agenda.adicionar(a2);
		agenda.adicionar(a3);
		
		System.out.println("Digite o nome: ");
		String nome = teclado.next();
		System.out.print("\n");
		System.out.println("Pesquisando\n\n");
		agenda.pesquisarContato(nome);
		System.out.println("Listando\n\n");
		agenda.listarContato();
		System.out.println("Removendo\n\n");
		agenda.removeContato(nome);
		System.out.println("Listando\n\n");
		agenda.listarContato();
		System.out.println("ordenando\n\n");
		agenda.ordenar();
		System.out.println("listando\n\n");
		agenda.listarContato();
		
		
        }
}