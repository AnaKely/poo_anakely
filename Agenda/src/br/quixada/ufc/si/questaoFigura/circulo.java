
package br.quixada.ufc.si.questaoFigura;

public class circulo {
     private String cor;
    
    
    public float AreaCiculo(int raio){
        
        float area = (float) (3.14 * (raio * raio));
        
        return area;
   }
    
    public float Perimetro(int raio){
        float pi = (float) 3.14;
        float perimetro = 2* pi;
        
        return perimetro;
   }
    
    public circulo(){
        
    }
    
    public circulo(String cor){
        this.cor = cor;
    }

    /**
     * @return the cor
     */
    public String getCor() {
        return cor;
    }

    /**
     * @param cor the cor to set
     */
    public void setCor(String cor) {
        this.cor = cor;
    }
}
