
package br.quixada.ufc.si.questaoFigura;


public class retangulo {
    private String cor;

    /**
     * @return the cor
     */
    public String getCor() {
        return cor;
    }

    /**
     * @param cor the cor to set
     */
    public void setCor(String cor) {
        this.cor = cor;
    }
    
    public int areaRetangulo(int lado, int base){
        int area = lado * base;
        
        return area;
    }
}
