
package br.quixada.ufc.si.model;

import java.time.LocalDate;


public class PessoaFisica extends Pessoa{
    private String nome;
    private String endereco;
    private String email;
    private int cpf;
    private LocalDate data_nasc;
    private String EstadoCivil;

    
    
    public PessoaFisica(){
        super();
    }
    
    public PessoaFisica(String nome,String endereco,String email,int cpf,LocalDate data_nasc, String EstadoCivil){
        this.nome = nome;
        this.endereco = endereco;
        this.email = email;
        this.data_nasc = data_nasc;
        this.EstadoCivil = EstadoCivil;
    }

   
    
    public String getNome() {
        return nome;
    }

    
    public void setNome(String nome) {
        this.nome = nome;
    }

    
    public String getEndereco() {
        return endereco;
    }

    
    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    
    public String getEmail() {
        return email;
    }

   
    public void setEmail(String email) {
        this.email = email;
    }

    
    public int getCpf() {
        return cpf;
    }

    
    public void setCpf(int cpf) {
        this.cpf = cpf;
    }

    public LocalDate getData_nasc() {
        return data_nasc;
    }

   
    public void setData_nasc(LocalDate data_nasc) {
        this.data_nasc = data_nasc;
    }

    public String getEstadoCivil() {
        return EstadoCivil;
    }

    public void setEstadoCivil(String EstadoCivil) {
        this.EstadoCivil = EstadoCivil;
    }
    
    @Override
     public String toString(){
        String pessoaF = "";
            pessoaF = "Nome: "+this.nome+"\n"+"Endereco: "+
                    this.endereco+"\n"+"Email: "+this.email+"CPF"+this.cpf+"Data de Nascimento"+this.data_nasc+"Estado Civil: "+this.EstadoCivil;
            return pessoaF;
    
        
    }   
}
