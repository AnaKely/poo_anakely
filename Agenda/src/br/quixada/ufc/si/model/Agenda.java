package br.quixada.ufc.si.model;



import java.util.ArrayList;


public class Agenda {

	public ArrayList<Pessoa> pessoas;
	
	
	

	public void ordenar() {
	
		for(int i = 0; i <pessoas.size(); i++) {
			int menor = i;
			for(int k = i + 1; k <pessoas.size();k++) {
				
				if(pessoas.get(k) instanceof PessoaFisica & pessoas.get(menor) instanceof PessoaFisica ) {
					if(((PessoaFisica) pessoas.get(k)).getCpf() < (((PessoaFisica) pessoas.get(menor)).getCpf())){
						menor = k;
					}
				}	
			}
			Pessoa aux = pessoas.get(i);
			pessoas.set(i, pessoas.get(menor));
			pessoas.set(menor, aux);
			
		}
		
		for(int i = 0; i <pessoas.size(); i++) {
			int pequeno = i;
			for(int k = i + 1; k <pessoas.size();k++) {
				
				if(pessoas.get(k) instanceof PessoaJuridica & pessoas.get(pequeno) instanceof PessoaJuridica ) {
					if(((PessoaJuridica) pessoas.get(k)).getCNPJ() < (((PessoaJuridica) pessoas.get(pequeno)).getCNPJ())){
						pequeno = k;
					}
				}	
			}
			Pessoa aux = pessoas.get(i);
			pessoas.set(i, pessoas.get(pequeno));
			pessoas.set(pequeno, aux);
			
		}
		System.out.println("Ordenado com sucesso.\n");
	}
	
	public void adicionar(Pessoa nome) {
	            pessoas.add(nome);
			}

	
	public void removeContato(String n) {
		System.out.println(n);
		for(int i= 0; i < pessoas.size(); i++) {
			Pessoa cara = pessoas.get(i);			
			if(cara.getNome().equals(n)) {
				pessoas.remove(cara);
				System.out.println("removido");
			break;
			}
		}
	}

	public void pesquisarContato(String nome) {
		int b = 0;
		for(int i= 0; i < pessoas.size(); i++) {
			Pessoa contatos = pessoas.get(i);
			if(contatos.getNome().equals(nome)) {
				System.out.println(contatos.toString());
				b = 1;
			}
		}
		if(b == 0) {
			System.out.println("Contato não encontrado");
		}
	}
	
	public void listarContato() {
		for(int i= 0; i < pessoas.size(); i++) {
			System.out.println(pessoas.get(i).toString());
		}
	}

   

}
