
package br.quixada.ufc.si.model;

import java.time.LocalDate;


public class PessoaJuridica extends Pessoa{
    private String nome;
    private String endereco;
    private String email;
    private int CNPJ;
    private int InscricaoEstadual;
    private String razaoSocial;

    
    public PessoaJuridica(){
        super();
    }
    
    public PessoaJuridica(String nome,String endereco,String email,int CNPJ,int InscricaoEstadual,String razaoSocial){
        this.nome = nome;
        this.endereco = endereco;
        this.email = email;
        this.CNPJ = CNPJ;
        this.InscricaoEstadual = InscricaoEstadual;
        this.razaoSocial = razaoSocial;
    }

   
    
    
    public String getNome() {
        return nome;
    }

    
    public void setNome(String nome) {
        this.nome = nome;
    }

   
    public String getEndereco() {
        return endereco;
    }

   
    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    
    public String getEmail() {
        return email;
    }

    
    public void setEmail(String email) {
        this.email = email;
    }

   
    public int getCNPJ() {
        return CNPJ;
    }

    public void setCNPJ(int CNPJ) {
        this.CNPJ = CNPJ;
    }

    public int getInscricaoEstadual() {
        return InscricaoEstadual;
    }

    
    public void setInscricaoEstadual(int InscricaoEstadual) {
        this.InscricaoEstadual = InscricaoEstadual;
    }

    
    public String getRazaoSocial() {
        return razaoSocial;
    }

    
    public void setRazaoSocial(String razaoSocial) {
        this.razaoSocial = razaoSocial;
    }
    
    @Override
    
    public String toString(){
        String pessoaJ = "";
            pessoaJ = "Nome: "+this.nome+"\n"+"Endereco: "+
                    this.endereco+"\n"+"Email: "+this.email+"CPF"+this.CNPJ+"Inscricao Estadual: "+this.InscricaoEstadual+"Razao social: "+this.razaoSocial;
            return pessoaJ;
    
        
    }      
}
