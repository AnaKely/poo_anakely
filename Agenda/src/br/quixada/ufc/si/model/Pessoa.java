
package br.quixada.ufc.si.model;


public class Pessoa {
    private String nome;
    private String endereco;
    private String email;

   
    public Pessoa(){
        
    }
    
    public Pessoa(String nome,String endereco, String email){
        this.nome = nome;
        this.endereco = endereco;
        this.email = email;
    }
    
    
    public String getNome() {
        return nome;
    }

   
    public void setNome(String nome) {
        this.nome = nome;
    }

   
    public String getEndereco() {
        return endereco;
    }

    
    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }
    
    
        public String toString(){
        String pessoa = "";
            pessoa = "Nome: "+this.nome+"\n"+"Endereco: "+
                    this.endereco+"\n"+"Email: "+this.email;
            return pessoa;
    
        
    }
}
