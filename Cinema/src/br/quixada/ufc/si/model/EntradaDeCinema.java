
package br.quixada.ufc.si.model;


public class EntradaDeCinema{
    private String titulo;
    private String horario;
    private String sala;
    private int poltrona;
    private float valor;
    private boolean disponivel;
    
    public EntradaDeCinema(String titulo, String horario, String sala, int poltrona, float valor, boolean disponivel) {
	this.titulo = titulo;
        this.horario = horario;
        this.sala = sala;
        this.poltrona = poltrona;
        this.valor = valor;
        this.disponivel = disponivel;
	}

   
    
    public String getTitulo(){
        return this.titulo;
}
    public String getHorario(){
        return this.horario;
    }
    public float getSala(){
        return this.valor;
        
}
    public int getPoltrona(){
        return this.poltrona;
    }
    
    public float calculaComDesconto(int dia, int mes, int ano){
        int idade = 2018 - ano;
        float valorAtual = (valor * 50)/100;
        
        if(idade<12){
            return valorAtual;        
        }
        else{
            return 0;
        }
     
    }
    public float calculaComDesconto(int dia, int mes, int ano, int nCarteira){
        int idade = 2018 - ano;
        float valorAtual1 = (valor * 40)/100;
        float valorAtual2 = (valor * 30)/100;
        float valorAtual3 = (valor * 20)/100;
        
        if(idade >=12 && idade <=15){
            return valorAtual1;
        }
        else if(idade >=16 && idade <=20){
            return valorAtual2;
        }
        else if(idade>20){
            return valorAtual3;
        }
        return 0;
        
    }
    public void realizarVenda(){
        if(disponivel == true){
            disponivel = false;
            System.out.println("Compra realizada com sucesso!");
        }
        else{
            System.out.println("Não foi possivel realizar a venda!");
        }
        
    }
    public String toString(){
        String cinema = "";
        cinema = "O titulo do filme: "+this.titulo+"\n"+ 
                    "Horario: "+this.horario+"\n"+
                    "Sala: "+this.sala+"\n"+
                    "Poltrona: "+this.poltrona+"\n"+
                    "Valor: "+this.valor+"\n"+
                    "Disponibilidade: "+this.disponivel;
        
        return cinema;
    }
}
   






